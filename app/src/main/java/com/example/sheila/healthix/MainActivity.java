package com.example.sheila.healthix;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    private static final String TAG = MainActivity.class.getSimpleName();
    private EditText emailText;
    private EditText passwordText;
    Button loginBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Log In");

        emailText = (EditText) findViewById(R.id.emailText);
        passwordText = (EditText) findViewById(R.id.passwordText);
    }

    //method to hide soft keyboard out of edittext
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            Log.d("Activity", "Touch event "+event.getRawX()+","+event.getRawY()+" "+x+","+y+" rect "+w.getLeft()+","+w.getTop()+","+w.getRight()+","+w.getBottom()+" coords "+scrcoords[0]+","+scrcoords[1]);
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom()) ) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }



    public void onClick(View view) {
        HashMap<String, String> postData = new HashMap<>();
        postData.put("username", emailText.getText().toString());
        postData.put("password", passwordText.getText().toString());

        new LoginTask().execute(postData);
    }

    private class LoginTask extends AsyncTask<HashMap<String, String>, Void, JSONObject> {

        private ProgressDialog progressDialog;

        public LoginTask() {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setTitle("Login");
            progressDialog.setMessage("Login in");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @SafeVarargs
        @Override
        protected final JSONObject doInBackground(HashMap<String, String>... params) {
            try {
                HashMap<String, String> hashMap = params[0];

                //URL to your API
                URL url = new URL("http://onboarding.healthixsolutions.com/api/v1/user/userlogin/");
                StringBuilder postData = new StringBuilder();

                // POST as urlencoded is basically key-value pairs, as with GET
                // This creates key=value&key=value&... pairs
                for (HashMap.Entry<String, String> param : hashMap.entrySet()) {
                    if (postData.length() != 0) {
                        postData.append('&');
                    }
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }

                Log.d(TAG, postData.toString());
                // Convert string to byte array, as it should be sent
                byte[] postDataBytes = postData.toString().getBytes("UTF-8");

                // Connect, easy
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                // Tell server that this is POST and in which format is the data
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);
                conn.setInstanceFollowRedirects(false);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                conn.setRequestProperty("User-Agent", "Mozilla/5.0 ( compatible ) ");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestProperty("Accept-Charset", "utf-8");
                conn.setRequestProperty("Connection", "keep-alive");
                conn.setRequestProperty("Host", "onboarding.healthixsolutions.com");
                conn.setRequestProperty("Accept-Encoding", "gzip,deflate");
                conn.getOutputStream().write(postDataBytes);
                if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    System.out.println("http response code is " + conn.getResponseCode());
                    return null;
                }
                // This gets the output from your server
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

                //read response to this string
                String response = "";
                String line;
                while ((line = in.readLine()) != null) {
                    response += line;
                }
                Log.d(TAG, response);
                return new JSONObject(response);
            } catch (Exception ignored) {
                Log.e(TAG, "Connection", ignored);
            }
            return null;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            if (progressDialog.isShowing()) {
                progressDialog.hide();
            }
            try {
                if (jsonObject != null) {
                    String success = jsonObject.getString("success");
                    if (success.equals("1")) {
                        //login successful start BmiCalculator activity
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage("Login successful")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(MainActivity.this,BmiCalculator.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }).create().show();

                    } else {
                        //login unsuccessful show error dialog
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Error")
                                .setMessage(jsonObject.getString("Error"))
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).create().show();
                    }
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Error")
                            .setMessage("Login error")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                }
            } catch (JSONException ignored) {
                ignored.printStackTrace();
            }
        }
    }


}
