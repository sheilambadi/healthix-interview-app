package com.example.sheila.healthix;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BmiCalculator extends AppCompatActivity {
    private EditText height;
    private EditText weight;
    Toolbar toolbarBmi;
    DatabaseHelper myDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi_calculator);

        //setting action bar
        toolbarBmi = (Toolbar)findViewById(R.id.toolbarBmi);
        setSupportActionBar(toolbarBmi);

        //set title on toolbar
        setTitle("Calculate BMI");

        //find the views
        height = (EditText)findViewById(R.id.heightText);
        weight = (EditText)findViewById(R.id.weightText);

        //initiate DatabaseHelper object
        myDB = new DatabaseHelper(this);

        //intent to go to listview
        Button viewList = (Button)findViewById(R.id.viewResultsButton);
        viewList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //opening activity with listview
                Intent intent = new Intent(BmiCalculator.this, ListResults.class);
                startActivity(intent);
                //placing focus on height EditText
                height.setFocusableInTouchMode(true);
                height.requestFocus();
            }
        });
    }

    //method to hide soft keyboard out of edittext
        @Override
        public boolean dispatchTouchEvent(MotionEvent event) {
            View v = getCurrentFocus();
            boolean ret = super.dispatchTouchEvent(event);

            if (v instanceof EditText) {
                View w = getCurrentFocus();
                int scrcoords[] = new int[2];
                w.getLocationOnScreen(scrcoords);
                float x = event.getRawX() + w.getLeft() - scrcoords[0];
                float y = event.getRawY() + w.getTop() - scrcoords[1];

                Log.d("Activity", "Touch event "+event.getRawX()+","+event.getRawY()+" "+x+","+y+" rect "+w.getLeft()+","+w.getTop()+","+w.getRight()+","+w.getBottom()+" coords "+scrcoords[0]+","+scrcoords[1]);
                if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom()) ) {

                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
                }
            }
            return ret;
        }

    public void CalculateBmi(View view){
        //get values entered by the user
        String heightStr = height.getText().toString();
        String weightStr = weight.getText().toString();

        //check if user has not entered values
        if(heightStr != null && !"".equals(heightStr)
                && weightStr != null && !"".equals(weightStr)) {
            //cast values entered to float
            float heightValue = Float.parseFloat(heightStr) / 100;
            float weightValue = Float.parseFloat(weightStr);

            float bmi = weightValue / (heightValue * heightValue);
            AddData(""+bmi);
            height.setText("");
            weight.setText("");
            height.setFocusableInTouchMode(true);
            height.requestFocus();
        } else {
            Toast.makeText(this, "Enter height and weight", Toast.LENGTH_LONG).show();
        }
    }


    public void viewResults(View view){
        //opening activity with listview
        Intent intent = new Intent(BmiCalculator.this, ListResults.class);
        startActivity(intent);
    }

    public void AddData(String newEntry){
        boolean insertData = myDB.addData(newEntry);

        //give user feedback on entry
        if(insertData==true){
            Toast.makeText(this, "BMI successfully inserted", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }
}