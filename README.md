# README #

Use http endpoints to authenticate login and validate for email and password. Have a BMI activity that a doctor can only access after login. Calculate BMI and save results in internal database and display results in a list.

### What is this repository for? ###

* Quick summary:  

 This is an interview question from Healthix, Nairobi Kenya.

* Version: 
 1.0

### How do I get set up? ###

* Summary of set up  

Have an android IDE to run the application on local machine. 

* Database configuration  

SQLite was used to store the BMIs calculated

* Deployment instructions  

Build the application and install the apk file from the app\build\outputs\apk folder. Choose the one that is not unaligned.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact